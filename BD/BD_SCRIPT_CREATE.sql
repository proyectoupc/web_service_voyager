USE [TURISMO]
GO
/****** Object:  Table [dbo].[Comment]    Script Date: 6/09/2019 15:35:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Comment](
	[IDComment] [bigint] IDENTITY(1,1) NOT NULL,
	[IDPlace] [bigint] NULL,
	[IDUser] [bigint] NULL,
	[Description] [varchar](1000) NULL,
	[Datetime] [datetime] NULL,
 CONSTRAINT [PK_Comentario] PRIMARY KEY CLUSTERED 
(
	[IDComment] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Follower]    Script Date: 6/09/2019 15:35:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Follower](
	[IDFollower] [bigint] IDENTITY(1,1) NOT NULL,
	[IDUser] [bigint] NULL,
	[IDUserFollower] [bigint] NULL,
	[DayFollowed] [datetime] NULL,
 CONSTRAINT [PK_Follower] PRIMARY KEY CLUSTERED 
(
	[IDFollower] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Photo]    Script Date: 6/09/2019 15:35:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Photo](
	[IDPhoto] [bigint] IDENTITY(1,1) NOT NULL,
	[IDPlace] [bigint] NULL,
	[Photo] [image] NULL,
PRIMARY KEY CLUSTERED 
(
	[IDPhoto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Place]    Script Date: 6/09/2019 15:35:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING OFF
GO
CREATE TABLE [dbo].[Place](
	[IDPlace] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](500) NULL,
	[Description] [varchar](max) NULL,
	[Tags] [varchar](500) NULL,
	[PostDateTime] [datetime] NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[Rating] [int] NULL,
	[Views] [int] NULL,
	[IDUser] [bigint] NOT NULL,
 CONSTRAINT [PK_Place] PRIMARY KEY CLUSTERED 
(
	[IDPlace] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RatingPlace]    Script Date: 6/09/2019 15:35:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RatingPlace](
	[IDRatingPlace] [bigint] IDENTITY(1,1) NOT NULL,
	[idPlace] [bigint] NULL,
	[idUser] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[IDRatingPlace] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SavePlace]    Script Date: 6/09/2019 15:35:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SavePlace](
	[IDSavePlace] [bigint] IDENTITY(1,1) NOT NULL,
	[IDUser] [bigint] NULL,
	[IDPlace] [bigint] NULL,
 CONSTRAINT [PK_SavePlace] PRIMARY KEY CLUSTERED 
(
	[IDSavePlace] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 6/09/2019 15:35:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[User](
	[IDUser] [bigint] IDENTITY(1,1) NOT NULL,
	[Email] [varchar](100) NOT NULL,
	[Password] [varchar](300) NULL,
	[Name] [varchar](250) NULL,
	[Lastname] [varchar](250) NULL,
	[Birthdate] [date] NULL,
	[Gender] [char](1) NULL,
	[Photo] [image] NULL,
	[Description] [varchar](1000) NULL,
	[BackgroundPhoto] [image] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[IDUser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ViewsPlace]    Script Date: 6/09/2019 15:35:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ViewsPlace](
	[IDViewsPlace] [bigint] IDENTITY(1,1) NOT NULL,
	[IDPlace] [bigint] NULL,
	[IDUser] [bigint] NULL,
PRIMARY KEY CLUSTERED 
(
	[IDViewsPlace] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
