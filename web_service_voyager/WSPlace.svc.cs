﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using web_service_voyager.Domain;
using web_service_voyager.Persistence;

namespace web_service_voyager
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WSPlace" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select WSPlace.svc or WSPlace.svc.cs at the Solution Explorer and start debugging.
    public class WSPlace : IWSPlace
    {
        PlaceDao dao = new PlaceDao();
        public Place Create(Place place)
        {
            return dao.Create(place);
        }

        public List<Place> GeSearchPlace(string condition)
        {
            UserDao daoUser = new UserDao();
            List<Place> places = dao.GeSearchPlace(condition);
            if (places != null && places.Count > 0)
            {
                for (int i = 0; i < places.Count; i++)
                {
                    places[i].user = daoUser.ObtainUserbyId(places[i].idUser);
                    places[i].idPhotos = dao.getPhotos(places[i].idPlace + "");
                }
            }
            return places;
        }

        public List<Place> getAll(String idUser)
        {
            List<Place> places = new List<Place>();
            try
            {
                UserDao daoUser = new UserDao();
                places = dao.getAll(idUser);
                if (places != null && places.Count > 0)
                {
                    for (int i = 0; i < places.Count; i++)
                    {
                        places[i].user = daoUser.ObtainUserbyId(places[i].idUser);
                        places[i].idPhotos = dao.getPhotos(places[i].idPlace + "");
                    }
                }
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
                Place place = new Place();
                place.description = e.Message;
                places.Add(place);
            }
            return places;
        }

        public List<Place> getAllUser(string idUser)
        {
            UserDao daoUser = new UserDao();
            List<Place> places = dao.getAllUser(idUser);
            if (places != null && places.Count > 0)
            {
                for (int i = 0; i < places.Count; i++)
                {
                    places[i].user = daoUser.ObtainUserbyId(places[i].idUser);
                    places[i].idPhotos = dao.getPhotos(places[i].idPlace + "");
                }
            }
            return places;
        }
    }
}
