﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using web_service_voyager.Domain;

namespace web_service_voyager.Persistence
{
    public class CommentDao
    {
        private string conexion = ConfigurationManager.ConnectionStrings["web_service_voyager.Properties.Settings.cn"].ConnectionString;
        public List<Comment> getAll(String idPlace)
        {
            List<Comment> comments = new List<Comment>();
            String sql = "SELECT c.[IDComment],c.[IDPlace],c.[IDUser],c.[Description],(CONVERT(VARCHAR(20),c.[Datetime],103)+' '+CONVERT(VARCHAR(20),c.[Datetime],108))[Datetime],u.Name + ' ' + u.Lastname AS userName  FROM [dbo].[Comment] c INNER JOIN dbo.[User] u ON u.IDUser = c.IDUser WHERE c.IDPlace=@idPlace order by IDComment desc";

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idPlace", idPlace));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        Comment comment;
                        while (dr.Read())
                        {
                            comment = new Comment()
                            {
                                idComentario = (Int64)dr["IDComment"],
                                idPlace = (Int64)dr["IDPlace"],
                                idUser = Int64.Parse(dr["IDUser"].ToString()),
                                description = dr["Description"].ToString(),
                                datetime = dr["Datetime"].ToString(),
                                userName = dr["userName"].ToString()
                            };
                            comments.Add(comment);
                        }
                    }
                }
            }
            return comments;
        }
        public int create(Comment comment)
        {
            String sql = "INSERT INTO [dbo].[Comment]([IDPlace],[IDUser],[Description],[Datetime])VALUES(@IDPlace,@IDUser,@DESCRIPTION,@Datetime); SELECT SCOPE_IDENTITY() as IDComment ";
            int index = 0;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@IDPlace", comment.idPlace));
                    comando.Parameters.Add(new SqlParameter("@IDUser", comment.idUser));
                    comando.Parameters.Add(new SqlParameter("@DESCRIPTION", comment.description));
                    DateTime dt = DateTime.ParseExact(comment.datetime, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    comando.Parameters.Add(new SqlParameter("@Datetime", dt));

                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        if (dr.Read())
                        { index = Int32.Parse(dr["IDComment"].ToString()); }
                    }
                }
            }
            return index;
        }
        public int delete(string idComment)
        {
            String sql = "DELETE FROM [dbo].[Comment] WHERE IDComment=@IDComment";
            int index = 0;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@IDComment", idComment));
                    index = comando.ExecuteNonQuery();
                }
            }
            return index;
        }
    }
}