﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using web_service_voyager.Domain;

namespace web_service_voyager.Persistence
{
    public class PlaceDao
    {
        private string conexion = ConfigurationManager.ConnectionStrings["web_service_voyager.Properties.Settings.cn"].ConnectionString;
        public List<Place> getAll(String idUser)
        {
            List<Place> places = new List<Place>();
            String sql = "SELECT p.[IDPlace],p.[Name],p.[Description],p.[Tags],(CONVERT(VARCHAR(20),p.[PostDateTime],103)+' '+CONVERT(VARCHAR(20),[PostDateTime],108))[PostDateTime],p.[Latitude],p.[Longitude],(SELECT COUNT(*) FROM dbo.RatingPlace WHERE idPlace=p.idplace)[Rating],(SELECT COUNT(*) FROM dbo.ViewsPlace WHERE idPlace=p.idplace)[Views],p.[IDUser],p.[ContactName],p.[ContactPhone],f.IDUser FROM [dbo].[Place] p INNER JOIN dbo.Follower F ON F.IDUser = p.IDUser WHERE f.IDUserFollower=@idUser UNION ALL SELECT p.[IDPlace],p.[Name],p.[Description],p.[Tags],(CONVERT(VARCHAR(20),p.[PostDateTime],103)+' '+CONVERT(VARCHAR(20),[PostDateTime],108))[PostDateTime],p.[Latitude],p.[Longitude],(SELECT COUNT(*) FROM dbo.RatingPlace WHERE idPlace=p.idplace)[Rating],(SELECT COUNT(*) FROM dbo.ViewsPlace WHERE idPlace=p.idplace)[Views],p.[IDUser],p.[ContactName],p.[ContactPhone],0 FROM [dbo].[Place] p WHERE p.IDUser=@idUser  ORDER BY p.IDPlace DESC";

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idUser", idUser));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        Place place;
                        while (dr.Read())
                        {
                            place = new Place()
                            {
                                idPlace = (Int64)dr["IDPlace"],
                                name = dr["Name"].ToString(),
                                description = dr["Description"].ToString(),
                                tags = dr["Name"].ToString(),
                                postDateTime = dr["PostDateTime"].ToString(),
                                latitude = float.Parse(dr["Latitude"].ToString()),
                                longitude = float.Parse(dr["Longitude"].ToString()),
                                rating = Int32.Parse(dr["Rating"].ToString()),
                                views = Int32.Parse(dr["Views"].ToString()),
                                idUser = Int64.Parse(dr["IDUser"].ToString()),
                                contactName = dr["contactName"].ToString(),
                                contactPhone = dr["contactPhone"].ToString(),
                            };
                            places.Add(place);
                        }
                    }
                }
            }
            return places;
        }
        public List<Place> GeSearchPlace(string condition)
        {
            List<Place> places = new List<Place>();
            String sql = "SELECT p.[IDPlace],p.[Name],p.[Description],p.[Tags],(CONVERT(VARCHAR(20),p.[PostDateTime],103)+' '+CONVERT(VARCHAR(20),p.[PostDateTime],108))[PostDateTime],p.[Latitude],p.[Longitude],(SELECT COUNT(*) FROM dbo.RatingPlace WHERE idPlace=p.idplace)[Rating],(SELECT COUNT(*) FROM dbo.ViewsPlace WHERE idPlace=p.idplace)[Views],p.[IDUser],p.[ContactName],p.[ContactPhone] FROM [dbo].[Place] p INNER JOIN dbo.[User] u ON u.IDUser = p.IDUser WHERE p.Name LIKE '%'+@condition+'%' OR p.Tags LIKE '%'+@condition+'%' OR u.Name LIKE '%'+@condition+'%' OR u.Lastname LIKE '%'+@condition+'%' ORDER BY p.IDPlace DESC";

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@condition", condition));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        Place place;
                        while (dr.Read())
                        {
                            place = new Place()
                            {
                                idPlace = (Int64)dr["IDPlace"],
                                name = dr["Name"].ToString(),
                                description = dr["Description"].ToString(),
                                tags = dr["Name"].ToString(),
                                postDateTime = dr["PostDateTime"].ToString(),
                                latitude = float.Parse(dr["Latitude"].ToString()),
                                longitude = float.Parse(dr["Longitude"].ToString()),
                                rating = Int32.Parse(dr["Rating"].ToString()),
                                views = Int32.Parse(dr["Views"].ToString()),
                                idUser = Int64.Parse(dr["IDUser"].ToString()),
                                contactName = dr["contactName"].ToString(),
                                contactPhone = dr["contactPhone"].ToString(),
                            };
                            places.Add(place);
                        }
                    }
                }
            }
            return places;
        }
        public Place getById(String idPlace)
        {
            Place place = null;
            String sql = "SELECT [IDPlace],[Name],[Description],[Tags],(CONVERT(VARCHAR(20),[PostDateTime],103)+' '+CONVERT(VARCHAR(20),[PostDateTime],108))[PostDateTime],[Latitude],[Longitude],(SELECT COUNT(*) FROM dbo.RatingPlace WHERE idPlace=p.idplace)[Rating],(SELECT COUNT(*) FROM dbo.ViewsPlace WHERE idPlace=p.idplace)[Views],[IDUser],[ContactName],[ContactPhone] FROM [dbo].[Place] p where idPlace=@idPlace ORDER BY p.IDPlace DESC";

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idPlace", idPlace));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            place = new Place()
                            {
                                idPlace = (Int64)dr["IDPlace"],
                                name = dr["Name"].ToString(),
                                description = dr["Description"].ToString(),
                                tags = dr["Name"].ToString(),
                                postDateTime = dr["PostDateTime"].ToString(),
                                latitude = float.Parse(dr["Latitude"].ToString()),
                                longitude = float.Parse(dr["Longitude"].ToString()),
                                rating = Int32.Parse(dr["Rating"].ToString()),
                                views = Int32.Parse(dr["Views"].ToString()),
                                idUser = Int64.Parse(dr["IDUser"].ToString()),
                                contactName = dr["contactName"].ToString(),
                                contactPhone = dr["contactPhone"].ToString(),
                            };
                        }
                    }
                }
            }
            return place;
        }
        public List<Place> getAllUser(String idUser)
        {
            List<Place> places = new List<Place>();
            String sql = "SELECT [IDPlace],[Name],[Description],[Tags],(CONVERT(VARCHAR(20),[PostDateTime],103)+' '+CONVERT(VARCHAR(20),[PostDateTime],108))[PostDateTime],[Latitude],[Longitude],(SELECT COUNT(*) FROM dbo.RatingPlace WHERE idPlace=p.idplace)[Rating],(SELECT COUNT(*) FROM dbo.ViewsPlace WHERE idPlace=p.idplace)[Views],[IDUser],[ContactName],[ContactPhone] FROM [dbo].[Place] p where p.IDUser=@IDUser ORDER BY PostDateTime DESC";

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@IDUser", idUser));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        Place place;
                        while (dr.Read())
                        {
                            place = new Place()
                            {
                                idPlace = (Int64)dr["IDPlace"],
                                name = dr["Name"].ToString(),
                                description = dr["Description"].ToString(),
                                tags = dr["Name"].ToString(),
                                postDateTime = dr["PostDateTime"].ToString(),
                                latitude = float.Parse(dr["Latitude"].ToString()),
                                longitude = float.Parse(dr["Longitude"].ToString()),
                                rating = Int32.Parse(dr["Rating"].ToString()),
                                views = Int32.Parse(dr["Views"].ToString()),
                                idUser = Int64.Parse(dr["IDUser"].ToString()),
                                contactName = dr["contactName"].ToString(),
                                contactPhone = dr["contactPhone"].ToString(),
                            };
                            places.Add(place);
                        }
                    }
                }
            }
            return places;
        }
        public Place Create(Place place)
        {
            String sql = "INSERT INTO [dbo].[Place]([Name],[Description],[Tags],[PostDateTime],[Latitude],[Longitude],[Rating],[Views],[IDUser],[ContactName],[ContactPhone])VALUES(@NAME,@DESCRIPTION,@Tags,@PostDateTime,@Latitude,@Longitude,@Rating,@VIEWS,@IDUser,@ContactName,@ContactPhone); SELECT SCOPE_IDENTITY() as IDPlace ";
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@NAME", place.name));
                    comando.Parameters.Add(new SqlParameter("@DESCRIPTION", place.description));
                    comando.Parameters.Add(new SqlParameter("@Tags", place.tags));
                    DateTime dt = DateTime.ParseExact(place.postDateTime, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    comando.Parameters.Add(new SqlParameter("@PostDateTime", dt));
                    comando.Parameters.Add(new SqlParameter("@Latitude", place.latitude));
                    comando.Parameters.Add(new SqlParameter("@Longitude", place.longitude));
                    comando.Parameters.Add(new SqlParameter("@Rating", place.rating));
                    comando.Parameters.Add(new SqlParameter("@VIEWS", place.views));
                    comando.Parameters.Add(new SqlParameter("@IDUser", place.idUser));

                    SqlParameter objPar = new SqlParameter("@ContactName", SqlDbType.VarChar);
                    if (!String.IsNullOrEmpty(place.contactName))
                    {
                        objPar.Value = place.contactName;
                    }
                    else
                    {
                        objPar.Value = DBNull.Value;
                    }
                    comando.Parameters.Add(objPar);
                    objPar = new SqlParameter("@ContactPhone", SqlDbType.VarChar);
                    if (!String.IsNullOrEmpty(place.contactPhone))
                    {
                        objPar.Value = place.contactPhone;
                    }
                    else
                    {
                        objPar.Value = DBNull.Value;
                    }
                    comando.Parameters.Add(objPar);

                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        if (dr.Read())
                        { place.idPlace = Int64.Parse(dr["IDPlace"].ToString()); }
                    }
                }
            }
            return place;
        }
        public List<Int64> getPhotos(String idPlace)
        {
            List<Int64> idPhotos = new List<Int64>();
            String sql = "SELECT IDPhoto FROM dbo.Photo WHERE IDPlace=@idPlace";

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idPlace", idPlace));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            idPhotos.Add((Int64)dr["IDPhoto"]);
                        }
                    }
                }
            }
            return idPhotos;
        }
    }
}