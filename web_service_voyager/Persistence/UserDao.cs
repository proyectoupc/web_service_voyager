﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using web_service_voyager.Dominio;

namespace web_service_voyager.Persistence
{
    public class UserDao
    {
        private string conexion = ConfigurationManager.ConnectionStrings["web_service_voyager.Properties.Settings.cn"].ConnectionString;
        public User login(User user)
        {
            User userObtained = null;
            String sql = "SELECT [IDUser],[Email],[Password],[Name],[Lastname],(CONVERT(VARCHAR(20),[Birthdate],103))[Birthdate],[Gender],[Photo],[Description],token FROM dbo.[User] WHERE Email=@email AND Password=@password;";

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@email", user.email));
                    comando.Parameters.Add(new SqlParameter("@password", user.password));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            userObtained = new User()
                            {
                                idUser = (Int64)dr["IDUser"],
                                email = dr["Email"].ToString(),
                                password = dr["Password"].ToString(),
                                name = dr["Name"].ToString(),
                                lastName = dr["Lastname"].ToString(),
                                birthday = dr["Birthdate"].ToString(),
                                gender = dr["Gender"].ToString(),
                                description = dr["Description"].ToString(),
                                token = dr["token"].ToString(),
                            };
                        }
                    }
                }
            }
            return userObtained;
        }

        public User Register(User user)
        {
            String sql = "INSERT dbo.[User] (Email, Password, Name, Lastname, Birthdate, Gender, Photo, Description ) VALUES(@Email, @Password, @Name, @Lastname, null, @Gender, @Photo, @Description )";

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {

                    SqlParameter objPar = new SqlParameter("@Email", SqlDbType.VarChar);
                    if (!String.IsNullOrEmpty(user.email))
                    {
                        objPar.Value = user.email;
                    }
                    else
                    {
                        objPar.Value = DBNull.Value;
                    }
                    comando.Parameters.Add(objPar);
                    objPar = new SqlParameter("@Password", SqlDbType.VarChar);
                    if (!String.IsNullOrEmpty(user.password))
                    {
                        objPar.Value = user.password;
                    }
                    else
                    {
                        objPar.Value = DBNull.Value;
                    }
                    comando.Parameters.Add(objPar);
                    objPar = new SqlParameter("@Name", SqlDbType.VarChar);
                    if (!String.IsNullOrEmpty(user.name))
                    {
                        objPar.Value = user.name;
                    }
                    else
                    {
                        objPar.Value = DBNull.Value;
                    }
                    comando.Parameters.Add(objPar);
                    objPar = new SqlParameter("@Lastname", SqlDbType.VarChar);
                    if (!String.IsNullOrEmpty(user.lastName))
                    {
                        objPar.Value = user.lastName;
                    }
                    else
                    {
                        objPar.Value = DBNull.Value;
                    }
                    comando.Parameters.Add(objPar);
                    objPar = new SqlParameter("@Gender", SqlDbType.VarChar);
                    if (!String.IsNullOrEmpty(user.gender))
                    {
                        objPar.Value = user.gender;
                    }
                    else
                    {
                        objPar.Value = DBNull.Value;
                    }
                    comando.Parameters.Add(objPar);
                    objPar = new SqlParameter("@Description", SqlDbType.VarChar);
                    if (!String.IsNullOrEmpty(user.description))
                    {
                        objPar.Value = user.description;
                    }
                    else
                    {
                        objPar.Value = DBNull.Value;
                    }
                    comando.Parameters.Add(objPar);

                    objPar = new SqlParameter("@Photo", SqlDbType.Image);
                    if (!String.IsNullOrEmpty(user.photo))
                    {
                        byte[] bFoto = Convert.FromBase64String(user.photo);
                        objPar.Value = bFoto;

                    }
                    else
                    {
                        objPar.Value = DBNull.Value;
                    }
                    comando.Parameters.Add(objPar);

                    comando.ExecuteNonQuery();


                }
            }
            return ObtainUserbyEmail(user.email);
        }

        public User ObtainUserbyEmail(String Email)
        {
            String sql = "SELECT [IDUser],[Email],[Password],[Name],[Lastname],(CONVERT(VARCHAR(20),[Birthdate],103))[Birthdate],[Gender],[Photo],[Description],token FROM dbo.[User] WHERE Email=@email;";
            User userObtained = null;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@email", Email));

                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            userObtained = new User()
                            {
                                idUser = (Int64)dr["IDUser"],
                                email = dr["Email"].ToString(),
                                password = dr["Password"].ToString(),
                                name = dr["Name"].ToString(),
                                lastName = dr["Lastname"].ToString(),
                                birthday = dr["Birthdate"].ToString(),
                                gender = dr["Gender"].ToString(),
                                //Photo = DateTime.Parse(dr["Photo"].ToString()),
                                description = dr["Description"].ToString(),
                                token = dr["token"].ToString(),
                            };
                        }
                    }
                }
            }
            return userObtained;
        }
        public User ObtainUserbyId(Int64 id)
        {
            String sql = "SELECT [IDUser],[Email],[Password],[Name],[Lastname],(CONVERT(VARCHAR(20),[Birthdate],103))[Birthdate],[Gender],[Photo],[Description],token FROM dbo.[User] WHERE IDUser=@id;";
            User userObtained = null;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@id", id));

                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            userObtained = new User()
                            {
                                idUser = (Int64)dr["IDUser"],
                                email = dr["Email"].ToString(),
                                password = dr["Password"].ToString(),
                                name = dr["Name"].ToString(),
                                lastName = dr["Lastname"].ToString(),
                                birthday = dr["Birthdate"].ToString(),
                                gender = dr["Gender"].ToString(),
                                //Photo = DateTime.Parse(dr["Photo"].ToString()),
                                description = dr["Description"].ToString(),
                                token = dr["token"].ToString(),
                            };
                        }
                    }
                }
            }
            return userObtained;
        }
        public User ObtainUserbyTokenReocveryPassword(String tokenRecoveryPassword)
        {
            String sql = "SELECT [IDUser],[Email],[Password],[Name],[Lastname],(CONVERT(VARCHAR(20),[Birthdate],103))[Birthdate],[Gender],[Photo],[Description],token FROM dbo.[User] WHERE tokenRecoveryPassword=@tokenRecoveryPassword;";
            User userObtained = null;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@tokenRecoveryPassword", tokenRecoveryPassword));

                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            userObtained = new User()
                            {
                                idUser = (Int64)dr["IDUser"],
                                email = dr["Email"].ToString(),
                                password = dr["Password"].ToString(),
                                name = dr["Name"].ToString(),
                                lastName = dr["Lastname"].ToString(),
                                birthday = dr["Birthdate"].ToString(),
                                gender = dr["Gender"].ToString(),
                                //Photo = DateTime.Parse(dr["Photo"].ToString()),
                                description = dr["Description"].ToString(),
                                token = dr["token"].ToString(),
                            };
                        }
                    }
                }
            }
            return userObtained;
        }
        public User Update(User user)
        {
            String sql = "UPDATE dbo.[User] SET Password = @Password,Name = @Name, Lastname = @Lastname, Birthdate = @Birthdate, Gender = @Gender, Description = @Description,Photo=@Photo,BackgroundPhoto=@BackgroundPhoto WHERE Email = @Email;";

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@Email", user.email));
                    comando.Parameters.Add(new SqlParameter("@Password", user.password));
                    comando.Parameters.Add(new SqlParameter("@Name", user.name));
                    comando.Parameters.Add(new SqlParameter("@Lastname", user.lastName));
                    comando.Parameters.Add(new SqlParameter("@Gender", user.gender));
                    comando.Parameters.Add(new SqlParameter("@Description", user.description));
                    DateTime dt = DateTime.ParseExact(user.birthday, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    comando.Parameters.Add(new SqlParameter("@Birthdate", dt));
                    SqlParameter objPar = new SqlParameter("@Photo", SqlDbType.Image);
                    if (!String.IsNullOrEmpty(user.photo))
                    {
                        byte[] bFoto = Convert.FromBase64String(user.photo);
                        objPar.Value = bFoto;
                    }
                    else
                    {
                        objPar.Value = DBNull.Value;
                    }
                    comando.Parameters.Add(objPar);
                    objPar = new SqlParameter("@BackgroundPhoto", SqlDbType.Image);
                    if (!String.IsNullOrEmpty(user.backgroundPhoto))
                    {
                        byte[] bFoto = Convert.FromBase64String(user.backgroundPhoto);
                        objPar.Value = bFoto;
                    }
                    else
                    {
                        objPar.Value = DBNull.Value;
                    }
                    comando.Parameters.Add(objPar);
                    comando.ExecuteNonQuery();
                }
            }
            return ObtainUserbyEmail(user.email);
        }
        public User UpdatePassword(User user)
        {
            String sql = "UPDATE dbo.[User] SET Password = @Password WHERE Email = @Email;";

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@Email", user.email));
                    comando.Parameters.Add(new SqlParameter("@Password", user.password));
                    
                    comando.ExecuteNonQuery();
                }
            }
            return ObtainUserbyEmail(user.email);
        }
        public int UpdateToken(User user)
        {
            String sql = "UPDATE dbo.[User] SET token = @token WHERE idUser = @idUser;";

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idUser", user.idUser));
                    comando.Parameters.Add(new SqlParameter("@token", user.token));
                    comando.ExecuteNonQuery();
                }
            }
            return 1;
        }
        public byte[] getPhotoById(String IDUser)
        {
            byte[] img = null;
            String sql = "SELECT [Photo] FROM [User] WHERE IDUser=@IDUser";
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@IDUser", IDUser));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            if (!dr.IsDBNull(dr.GetOrdinal("Photo")))
                            {
                                img = (byte[])dr.GetSqlBinary(dr.GetOrdinal("Photo"));
                            }
                        };
                    }
                }
            }
            return img;
        }
        public byte[] getBackgroundPhotoById(String IDUser)
        {
            byte[] img = null;
            String sql = "SELECT [BackgroundPhoto] FROM [User] WHERE IDUser=@IDUser";
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@IDUser", IDUser));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            if (!dr.IsDBNull(dr.GetOrdinal("BackgroundPhoto")))
                            {
                                img = (byte[])dr.GetSqlBinary(dr.GetOrdinal("BackgroundPhoto"));
                            }
                        };
                    }
                }
            }
            return img;
        }
        public int updateTokenRecoveryPassword(String email, String token)
        {
            String sql = "UPDATE dbo.[User] SET tokenRecoveryPassword = @tokenRecoveryPassword WHERE Email = @Email;";
            int response = 0;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    if (!String.IsNullOrEmpty(token))
                    {
                        comando.Parameters.Add(new SqlParameter("@tokenRecoveryPassword", token));
                    }
                    else
                    {
                        comando.Parameters.Add(new SqlParameter("@tokenRecoveryPassword", DBNull.Value));
                    }
                    comando.Parameters.Add(new SqlParameter("@email", email));
                    response = comando.ExecuteNonQuery();
                }
            }
            return response;
        }
    }
}