﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using web_service_voyager.Domain;

namespace web_service_voyager.Persistence
{
    public class PhotoDao
    {
        private string conexion = ConfigurationManager.ConnectionStrings["web_service_voyager.Properties.Settings.cn"].ConnectionString;
        public int Create(Photo photo)
        {
            String sql = "INSERT INTO [dbo].[Photo]([IDPlace],[Photo])VALUES(@IDPlace,@Photo)";
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@IDPlace", photo.idPlace));
                    if (!String.IsNullOrEmpty(photo.photo))
                    {
                        SqlParameter objPar = new SqlParameter("@Photo", SqlDbType.Image);
                        byte[] bFoto = Convert.FromBase64String(photo.photo);
                        objPar.Value = bFoto;
                        comando.Parameters.Add(objPar);
                    }
                    comando.ExecuteNonQuery();
                }
            }
            return 1;
        }
        public byte[] getPhotoById(String idPhoto)
        {
            byte[] img = null;
            String sql = "SELECT [Photo] FROM [Photo]	WHERE IDPhoto=@idPhoto";
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idPhoto", idPhoto));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            img = (byte[])dr.GetSqlBinary(dr.GetOrdinal("Photo"));
                        };
                    }
                }
            }
            return img;
        }
    }
}