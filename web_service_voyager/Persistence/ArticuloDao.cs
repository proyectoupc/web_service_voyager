﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using web_service_voyager.Domain;

namespace web_service_voyager.Persistence
{
    public class ArticuloDao
    {
        private string conexion = ConfigurationManager.ConnectionStrings["web_service_voyager.Properties.Settings.cn"].ConnectionString;
        public List<Articulo> getAll()
        {
            List<Articulo> articulos = new List<Articulo>();
            String sql = "SELECT * from articulo";
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        Articulo articulo;
                        while (dr.Read())
                        {
                            articulo = new Articulo()
                            {
                                idArticulo = (Int32)dr["idArticulo"],
                                nombre = dr["nombre"].ToString(),
                                descripcion = dr["descripcion"].ToString(),
                                precio = Decimal.Parse(dr["precio"].ToString()),
                                stock = (Int32)dr["stock"]
                            };
                            articulos.Add(articulo);
                        }
                    }
                }
            }
            return articulos;
        }
        public int create(Articulo articulo)
        {
            String sql = "INSERT INTO [dbo].[Articulo]([nombre],[descripcion],[precio],[stock])VALUES(@nombre,@descripcion,@precio,@stock)";
            int index = 0;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@nombre", articulo.nombre));
                    comando.Parameters.Add(new SqlParameter("@descripcion", articulo.descripcion));
                    comando.Parameters.Add(new SqlParameter("@precio", articulo.precio));
                    comando.Parameters.Add(new SqlParameter("@stock", articulo.stock));
                    comando.ExecuteNonQuery();
                }
            }
            return index;
        }
        public int update(Articulo articulo)
        {
            String sql = "UPDATE [dbo].[Articulo] SET [nombre] = @nombre,[descripcion] = @descripcion, [precio] = @precio,[stock] = @stock WHERE idArticulo=@idArticulo";
            int index = 0;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idArticulo", articulo.idArticulo));
                    comando.Parameters.Add(new SqlParameter("@nombre", articulo.nombre));
                    comando.Parameters.Add(new SqlParameter("@descripcion", articulo.descripcion));
                    comando.Parameters.Add(new SqlParameter("@precio", articulo.precio));
                    comando.Parameters.Add(new SqlParameter("@stock", articulo.stock));
                    comando.ExecuteNonQuery();
                }
            }
            return index;
        }
        public int delete(string idArticulo)
        {
            String sql = "DELETE FROM [dbo].[Articulo] WHERE idArticulo=@idArticulo";
            int index = 0;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idArticulo", idArticulo));
                    comando.ExecuteNonQuery();
                }
            }
            return index;
        }
    }
}