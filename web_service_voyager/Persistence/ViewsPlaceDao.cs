﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using web_service_voyager.Domain;

namespace web_service_voyager.Persistence
{
    public class ViewsPlaceDao
    {
        private string conexion = ConfigurationManager.ConnectionStrings["web_service_voyager.Properties.Settings.cn"].ConnectionString;
        public ViewsPlace get(String idPlace, String idUser)
        {
            ViewsPlace viewPlace = null;
            String sql = "SELECT [IDViewsPlace],[IDPlace],[IDUser] FROM [ViewsPlace] WHERE [idPlace]=@idPlace AND [idUser]=@idUser";

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idPlace", idPlace));
                    comando.Parameters.Add(new SqlParameter("@idUser", idUser));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {

                        if (dr.Read())
                        {
                            viewPlace = new ViewsPlace()
                            {
                                idViewsPlace = (Int64)dr["IDViewsPlace"],
                                idPlace = (Int64)dr["IDPlace"],
                                idUser = Int64.Parse(dr["IDUser"].ToString()),
                            };
                        }
                    }
                }
            }
            return viewPlace;
        }
        public ViewsPlace create(ViewsPlace viewPlace)
        {
            String sql = "INSERT INTO [dbo].[ViewsPlace]([idPlace],[idUser])VALUES(@idPlace, @idUser)";
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idPlace", viewPlace.idPlace));
                    comando.Parameters.Add(new SqlParameter("@idUser", viewPlace.idUser));
                    comando.ExecuteNonQuery();
                }
            }
            return get(viewPlace.idPlace + "", viewPlace.idUser + "");
        }
        public int delete(string idViewPlace)
        {
            String sql = "DELETE FROM [dbo].[ViewsPlace] WHERE IDViewsPlace=@IDViewsPlace";
            int index = 0;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@IDViewsPlace", idViewPlace));
                    index = comando.ExecuteNonQuery();
                }
            }
            return index;
        }
        public Int64 getCount(String idPlace)
        {
            Int64 viewPlace = 0;
            String sql = "SELECT count(*) as countViews FROM [ViewsPlace] WHERE [idPlace]=@idPlace";
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idPlace", idPlace));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            viewPlace = Int64.Parse(dr["countViews"].ToString());
                        }
                    }
                }
            }
            return viewPlace;
        }
    }
}