﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using web_service_voyager.Domain;

namespace web_service_voyager.Persistence
{
    public class SavePlaceDao
    {
        private string conexion = ConfigurationManager.ConnectionStrings["web_service_voyager.Properties.Settings.cn"].ConnectionString;
        public List<SavePlace> getAll(String idUser)
        {
            List<SavePlace> savePlaces = new List<SavePlace>();
            String sql = "SELECT sp.[IDSavePlace],p.[IDPlace],p.[Name],p.[Description],p.[Tags],(CONVERT(VARCHAR(20),p.[PostDateTime],103)+' '+CONVERT(VARCHAR(20),p.[PostDateTime],108))[PostDateTime],p.[Latitude],p.[Longitude],(SELECT COUNT(*) FROM dbo.RatingPlace WHERE idPlace=p.idplace)[Rating],(SELECT COUNT(*) FROM dbo.ViewsPlace WHERE idPlace=p.idplace)[Views],p.[IDUser],p.[ContactName],p.[ContactPhone] FROM [SavePlace] sp INNER JOIN dbo.Place p ON p.IDPlace = sp.IDPlace WHERE sp.IDUser=@iduser order by IDSavePlace desc";

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@iduser", idUser));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        SavePlace savePlace;
                        while (dr.Read())
                        {
                            savePlace = new SavePlace()
                            {
                                idSavePlace = (Int64)dr["IDSavePlace"],
                                idPlace = (Int64)dr["IDPlace"],
                                idUser = Int64.Parse(idUser),
                                place = new Place()
                                {
                                    idPlace = (Int64)dr["IDPlace"],
                                    name = dr["Name"].ToString(),
                                    description = dr["Description"].ToString(),
                                    tags = dr["Tags"].ToString(),
                                    postDateTime = dr["PostDateTime"].ToString(),
                                    latitude = float.Parse(dr["Latitude"].ToString()),
                                    longitude = float.Parse(dr["Longitude"].ToString()),
                                    rating = Int32.Parse(dr["Rating"].ToString()),
                                    views = Int32.Parse(dr["Views"].ToString()),
                                    idUser = Int64.Parse(dr["IDUser"].ToString()),
                                }
                            };
                            savePlaces.Add(savePlace);
                        }
                    }
                }
            }
            return savePlaces;
        }
        public SavePlace get(String idUser, String idPlace)
        {
            SavePlace savePlace = null;
            String sql = "SELECT sp.[IDSavePlace],p.[IDPlace],p.[Name],p.[Description],p.[Tags],(CONVERT(VARCHAR(20),p.[PostDateTime],103)+' '+CONVERT(VARCHAR(20),p.[PostDateTime],108))[PostDateTime],p.[Latitude],p.[Longitude],(SELECT COUNT(*) FROM dbo.RatingPlace WHERE idPlace=p.idplace)[Rating],(SELECT COUNT(*) FROM dbo.ViewsPlace WHERE idPlace=p.idplace)[Views],p.[IDUser],p.[ContactName],p.[ContactPhone] FROM [SavePlace] sp INNER JOIN dbo.Place p ON p.IDPlace = sp.IDPlace WHERE sp.IDUser=@iduser and sp.idPlace=@idPlace";

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@iduser", idUser));
                    comando.Parameters.Add(new SqlParameter("@idPlace", idPlace));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            savePlace = new SavePlace()
                            {
                                idSavePlace = (Int64)dr["IDSavePlace"],
                                idPlace = (Int64)dr["IDPlace"],
                                idUser = Int64.Parse(idUser),
                                place = new Place()
                                {
                                    idPlace = (Int64)dr["IDPlace"],
                                    name = dr["Name"].ToString(),
                                    description = dr["Description"].ToString(),
                                    tags = dr["Tags"].ToString(),
                                    postDateTime = dr["PostDateTime"].ToString(),
                                    latitude = float.Parse(dr["Latitude"].ToString()),
                                    longitude = float.Parse(dr["Longitude"].ToString()),
                                    rating = Int32.Parse(dr["Rating"].ToString()),
                                    views = Int32.Parse(dr["Views"].ToString()),
                                    idUser = Int64.Parse(dr["IDUser"].ToString()),
                                }
                            };
                        }
                    }
                }
            }
            return savePlace;
        }
        public int create(SavePlace savePlace)
        {
            String sql = "INSERT INTO [dbo].[SavePlace] ([IDUser],[IDPlace])VALUES(@IDUser,@IDPlace)";
            int index = 0;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@IDUser", savePlace.idUser));
                    comando.Parameters.Add(new SqlParameter("@IDPlace", savePlace.idPlace));
                    index = comando.ExecuteNonQuery();
                }
            }
            return index;
        }
        public int delete(string idSavePlace)
        {
            String sql = "DELETE FROM [dbo].[SavePlace] WHERE IDSavePlace=@idSavePlace";
            int index = 0;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idSavePlace", idSavePlace));
                    index = comando.ExecuteNonQuery();
                }
            }
            return index;
        }
    }
}