﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using web_service_voyager.Domain;

namespace web_service_voyager.Persistence
{
    public class RatingPlaceDao
    {
        private string conexion = ConfigurationManager.ConnectionStrings["web_service_voyager.Properties.Settings.cn"].ConnectionString;
        public RatingPlace get(String idPlace, String idUser)
        {
            RatingPlace rating = null;
            String sql = "SELECT [IDRatingPlace],[idPlace],[idUser] FROM [RatingPlace] WHERE [idPlace]=@idPlace AND [idUser]=@idUser";

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idPlace", idPlace));
                    comando.Parameters.Add(new SqlParameter("@idUser", idUser));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {

                        if (dr.Read())
                        {
                            rating = new RatingPlace()
                            {
                                idRatingPlace = (Int64)dr["IDRatingPlace"],
                                idPlace = (Int64)dr["IDPlace"],
                                idUser = Int64.Parse(dr["IDUser"].ToString()),
                            };
                        }
                    }
                }
            }
            return rating;
        }
        public Int64 getCount(String idPlace)
        {
            Int64 ratingCount = 0;
            String sql = "SELECT count(*) countViews FROM [RatingPlace] WHERE [idPlace]=@idPlace";
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idPlace", idPlace));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            ratingCount = Int64.Parse(dr["countViews"].ToString());

                        }
                    }
                }
            }
            return ratingCount;
        }
        public RatingPlace create(RatingPlace ratingPlace)
        {
            String sql = "INSERT INTO [dbo].[RatingPlace]([idPlace],[idUser])VALUES(@idPlace, @idUser)";
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idPlace", ratingPlace.idPlace));
                    comando.Parameters.Add(new SqlParameter("@idUser", ratingPlace.idUser));
                    comando.ExecuteNonQuery();
                }
            }
            return get(ratingPlace.idPlace + "", ratingPlace.idUser + "");
        }
        public int delete(string idRatingPlace)
        {
            String sql = "DELETE FROM [dbo].[RatingPlace] WHERE IDRatingPlace=@idRatingPlace";
            int index = 0;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idRatingPlace", idRatingPlace));
                    index = comando.ExecuteNonQuery();
                }
            }
            return index;
        }
    }
}