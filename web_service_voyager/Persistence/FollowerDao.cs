﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using web_service_voyager.Domain;

namespace web_service_voyager.Persistence
{
    public class FollowerDao
    {
        private string conexion = ConfigurationManager.ConnectionStrings["web_service_voyager.Properties.Settings.cn"].ConnectionString;
        public List<Follower> getAll(String idUser)
        {
            List<Follower> followers = new List<Follower>();
            String sql = "SELECT F.[IDFollower],F.[IDUser],F.[IDUserFollower],(CONVERT(VARCHAR(20),F.[DayFollowed],103)+' '+CONVERT(VARCHAR(20),F.[DayFollowed],108))[DayFollowed],u.Name + ' ' + u.Lastname AS userName FROM [Follower] F INNER JOIN dbo.[User] U ON U.IDUser = F.IDUserFollower WHERE f.IDUser=@idUser";

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idUser", idUser));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        Follower follower;
                        while (dr.Read())
                        {
                            follower = new Follower()
                            {
                                idFollower = (Int64)dr["IDFollower"],
                                idUser = Int64.Parse(dr["IDUser"].ToString()),
                                idUserFollower = Int64.Parse(dr["IDUserFollower"].ToString()),
                                dayFollowed = dr["DayFollowed"].ToString(),
                                userName = dr["userName"].ToString()
                            };
                            followers.Add(follower);
                        }
                    }
                }
            }
            return followers;
        }

        internal Follower Get(String idUser, String IDUserFollower)
        {
            Follower follower = null;
            String sql = "SELECT [IDFollower],[IDUser],[IDUserFollower],[DayFollowed] FROM [dbo].[Follower] WHERE IDUser=@idUser and IDUserFollower=@IDUserFollower";

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idUser", idUser));
                    comando.Parameters.Add(new SqlParameter("@IDUserFollower", IDUserFollower));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            follower = new Follower
                            {
                                idFollower = (Int64)dr["IDFollower"],
                                idUserFollower = Int64.Parse(dr["IDUser"].ToString()),
                                idUser = Int64.Parse(dr["IDUserFollower"].ToString()),
                                dayFollowed = dr["DayFollowed"].ToString()
                            };

                        }
                    }

                }
            }
            return follower;
        }

        public List<Follower> getAllFollows(String idUser)
        {
            List<Follower> followers = new List<Follower>();
            String sql = "SELECT F.[IDFollower],F.[IDUser],F.[IDUserFollower],(CONVERT(VARCHAR(20),F.[DayFollowed],103)+' '+CONVERT(VARCHAR(20),F.[DayFollowed],108))[DayFollowed],u.Name + ' ' + u.Lastname AS userName FROM [Follower] F INNER JOIN dbo.[User] U ON U.IDUser = F.IDUser WHERE f.IDUserFollower=@idUser";

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idUser", idUser));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        Follower follower;
                        while (dr.Read())
                        {
                            follower = new Follower()
                            {
                                idFollower = (Int64)dr["IDFollower"],
                                idUserFollower = Int64.Parse(dr["IDUser"].ToString()),
                                idUser = Int64.Parse(dr["IDUserFollower"].ToString()),
                                dayFollowed = dr["DayFollowed"].ToString(),
                                userName = dr["userName"].ToString()
                            };
                            followers.Add(follower);
                        }
                    }
                }
            }
            return followers;
        }

        public int create(Follower follower)
        {
            String sql = "INSERT INTO [dbo].[Follower]([IDUser],[IDUserFollower],[DayFollowed])VALUES(@IDUser, @IDUserFollower,@DayFollowed); SELECT SCOPE_IDENTITY() as IDFollower";
            int index = 0;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@IDUser", follower.idUser));
                    comando.Parameters.Add(new SqlParameter("@IDUserFollower", follower.idUserFollower));
                    DateTime dt = DateTime.ParseExact(follower.dayFollowed, "dd/MM/yyyy HH:mm:ss", System.Globalization.CultureInfo.InvariantCulture);
                    comando.Parameters.Add(new SqlParameter("@DayFollowed", dt));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        if (dr.Read())
                        { index = Int32.Parse(dr["IDFollower"].ToString()); }
                    }
                }
            }
            return index;
        }
        public int delete(string idFollower)
        {
            String sql = "DELETE FROM [dbo].[Follower] WHERE IDFollower=@idFollower";
            int index = 0;
            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idFollower", idFollower));
                    index = comando.ExecuteNonQuery();
                }
            }
            return index;
        }
        public int getCount(String idUser)
        {
            int followers = 0;
            String sql = "SELECT count(*)countFollows FROM [Follower] F  WHERE f.IDUser=@idUser";

            using (SqlConnection cn = new SqlConnection(conexion))
            {
                cn.Open();
                using (SqlCommand comando = new SqlCommand(sql, cn))
                {
                    comando.Parameters.Add(new SqlParameter("@idUser", idUser));
                    using (SqlDataReader dr = comando.ExecuteReader())
                    {
                        if (dr.Read())
                        {
                            followers = (int)dr["countFollows"];
                        }
                    }
                }
            }
            return followers;
        }
    }
}