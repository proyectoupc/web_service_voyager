﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using web_service_voyager.Domain;
using web_service_voyager.Persistence;

namespace web_service_voyager
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "WSArticulo" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione WSArticulo.svc o WSArticulo.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class WSArticulo : IWSArticulo
    {
        private ArticuloDao dao = new ArticuloDao();
        public int Create(Articulo comment)
        {
            return dao.create(comment);
        }

        public int Delete(string idArticulo)
        {
            return dao.delete(idArticulo);
        }

        public List<Articulo> GetAll()
        {
            return dao.getAll();
        }

        public int Update(Articulo comment)
        {
            return dao.update(comment);
        }
    }
}
