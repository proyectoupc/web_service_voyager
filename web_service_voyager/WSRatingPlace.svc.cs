﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using web_service_voyager.Domain;
using web_service_voyager.Persistence;

namespace web_service_voyager
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "WSRatingPlace" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione WSRatingPlace.svc o WSRatingPlace.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class WSRatingPlace : IWSRatingPlace
    {
        RatingPlaceDao dao = new RatingPlaceDao();
        public RatingPlace Create(RatingPlace ratingPlace)
        {
            RatingPlace ratingPlaceCreated = dao.get(ratingPlace.idPlace + "", ratingPlace.idUser + "");
            if (ratingPlaceCreated == null)
            {
                ratingPlaceCreated = dao.create(ratingPlace);
            }
            ratingPlaceCreated.countLikes = dao.getCount(ratingPlaceCreated.idPlace + "");
            return ratingPlaceCreated;
        }

        public Int64 Delete(string idRatingPlace, string idPlace)
        {
            dao.delete(idRatingPlace);
            return dao.getCount(idPlace);
        }

        public RatingPlace Get(string idPlace, string idUser)
        {
            return dao.get(idPlace, idUser);
        }
    }
}
