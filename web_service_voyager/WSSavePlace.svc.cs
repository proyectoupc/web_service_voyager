﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using web_service_voyager.Domain;
using web_service_voyager.Persistence;

namespace web_service_voyager
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "WSSavePlace" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione WSSavePlace.svc o WSSavePlace.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class WSSavePlace : IWSSavePlace
    {
        SavePlaceDao dao = new SavePlaceDao();

        public int Create(SavePlace savePlace)
        {
            SavePlace savePlaceCreate = dao.get(savePlace.idUser.ToString(), savePlace.idPlace.ToString());
            if (savePlaceCreate == null)
            {
                dao.create(savePlace);
            }
            return 1;
        }

        public int Delete(string idSavePlace)
        {
            return dao.delete(idSavePlace);
        }

        public List<SavePlace> GetAll(string idUser)
        {
            UserDao daoUser = new UserDao();
            PlaceDao daoPlace = new PlaceDao();
            List<SavePlace> places = dao.getAll(idUser);
            for (int i = 0; i < places.Count; i++)
            {
                places[i].place.user = daoUser.ObtainUserbyId(places[i].place.idUser);
                places[i].place.idPhotos = daoPlace.getPhotos(places[i].idPlace + "");
            }
            return places;
        }
    }
}
