﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using web_service_voyager.Domain;

namespace web_service_voyager
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IWSViewsPlace" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IWSViewsPlace
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Get/{idPlace}/{idUser}", ResponseFormat = WebMessageFormat.Json)]
        ViewsPlace Get(String idPlace, String idUser);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Create", ResponseFormat = WebMessageFormat.Json)]
        ViewsPlace Create(ViewsPlace ratingPlace);

        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "Delete/{idViewsPlace}", ResponseFormat = WebMessageFormat.Json)]
        int Delete(string idViewsPlace);
    }
}
