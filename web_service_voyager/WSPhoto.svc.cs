﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using web_service_voyager.Domain;
using web_service_voyager.Persistence;

namespace web_service_voyager
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "WSPhoto" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione WSPhoto.svc o WSPhoto.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class WSPhoto : IWSPhoto
    {
        PhotoDao dao = new PhotoDao();
        public int Create(Photo photo)
        {
            return dao.Create(photo);
        }

        public Stream GetPhoto(string idPhoto)
        {
            MemoryStream ms = new MemoryStream(dao.getPhotoById(idPhoto));
            WebOperationContext.Current.OutgoingRequest.Headers.Add("Slug", "title");
            WebOperationContext.Current.OutgoingRequest.Method = "GET";
            WebOperationContext.Current.OutgoingResponse.ContentType = "image/jpeg";
            return ms;
        }
    }
}
