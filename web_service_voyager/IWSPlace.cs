﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using web_service_voyager.Domain;

namespace web_service_voyager
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWSPlace" in both code and config file together.
    [ServiceContract]
    public interface IWSPlace
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAll/{idUser}", ResponseFormat = WebMessageFormat.Json)]
        List<Place> getAll(String idUser);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAllUser/{idUser}", ResponseFormat = WebMessageFormat.Json)]
        List<Place> getAllUser(String idUser);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Create", ResponseFormat = WebMessageFormat.Json)]
        Place Create(Place place);
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GeSearchPlace/{condition}", ResponseFormat = WebMessageFormat.Json)]
        List<Place> GeSearchPlace(String condition);
    }
}
