﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_service_voyager.Domain
{
    public class Articulo
    {
        public int idArticulo { get; set; }
        public String nombre { get; set; }
        public String descripcion { get; set; }
        public Decimal precio { get; set; }
        public Int32 stock { get; set; }
    }
}