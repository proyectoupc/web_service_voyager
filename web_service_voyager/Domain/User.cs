﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace web_service_voyager.Dominio
{
    [DataContract]
    public class User
    {
        [DataMember]
        public Int64 idUser { get; set; }
        [DataMember]
        public String email { get; set; }
        [DataMember]
        public String password { get; set; }
        [DataMember]
        public String name { get; set; }
        [DataMember]
        public String lastName { get; set; }
        [DataMember]
        public String birthday { get; set; }
        [DataMember]
        public String gender { get; set; }
        [DataMember]
        public String photo { get; set; }
        [DataMember]
        public String description { get; set; }
        [DataMember]
        public String backgroundPhoto { get; set; }
        [DataMember]
        public String token { get; set; }
    }
}