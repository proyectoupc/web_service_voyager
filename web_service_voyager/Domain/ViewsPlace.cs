﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_service_voyager.Domain
{
    public class ViewsPlace
    {
        public Int64 idViewsPlace { get; set; }
        public Int64 idPlace { get; set; }
        public Int64 idUser { get; set; }
        public Int64 countViews { get; set; }
    }
}