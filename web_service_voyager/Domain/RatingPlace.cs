﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace web_service_voyager.Domain
{
    public class RatingPlace
    {
        public Int64 idRatingPlace { get; set; }
        public Int64 idPlace { get; set; }
        public Int64 idUser { get; set; }
        public Int64 countLikes { get; set; }
    }
}