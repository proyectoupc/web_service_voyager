﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace web_service_voyager.Domain
{
    [DataContract]
    public class Photo
    {
        [DataMember]
        public Int64 idPhoto { get; set; }
        [DataMember]
        public Int64 idPlace { get; set; }
        [DataMember]
        public String photo { get; set; }
    }
}