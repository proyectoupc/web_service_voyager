﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using web_service_voyager.Dominio;

namespace web_service_voyager.Domain
{
    public class SavePlace
    {
        public Int64 idSavePlace { get; set; }
        public Int64 idUser { get; set; }
        public Int64 idPlace { get; set; }
        public Place place { get; set; }

    }
}