﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using web_service_voyager.Dominio;

namespace web_service_voyager.Domain
{
    [DataContract]
    public class Place
    {
        [DataMember]
        public Int64 idPlace { get; set; }
        [DataMember]
        public String name { get; set; }
        [DataMember]
        public String description { get; set; }
        [DataMember]
        public String tags { get; set; }
        [DataMember]
        public String postDateTime { get; set; }
        [DataMember]
        public float latitude { get; set; }
        [DataMember]
        public float longitude { get; set; }
        [DataMember]
        public int rating { get; set; }
        [DataMember]
        public int views { get; set; }
        [DataMember]
        public Int64 idUser { get; set; }
        [DataMember]
        public User user { get; set; }
        [DataMember]
        public String contactName { get; set; }
        [DataMember]
        public String contactPhone { get; set; }
        [DataMember]
        public List<Int64> idPhotos { get; set; }
    }
}