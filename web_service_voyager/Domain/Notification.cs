﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using web_service_voyager.Dominio;

namespace web_service_voyager.Domain
{
    public class Notification
    {
        public User user { get; set; }
        public Place place { get; set; }
        public Comment comment { get; set; }
    }
}