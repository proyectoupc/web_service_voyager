﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using web_service_voyager.Dominio;

namespace web_service_voyager.Domain
{
    public class Comment
    {
        public Int64 idComentario { get; set; }
        public Int64 idPlace { get; set; }
        public Int64 idUser { get; set; }
        public String description { get; set; }
        public String datetime { get; set; }
        public String userName { get; set; }
        public User user { get; set; }
    }
}