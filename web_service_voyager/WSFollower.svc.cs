﻿using System;
using System.Collections.Generic;
using web_service_voyager.Domain;
using web_service_voyager.Persistence;

namespace web_service_voyager
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "WSFollower" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione WSFollower.svc o WSFollower.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class WSFollower : IWSFollower
    {
        FollowerDao dao = new FollowerDao();
        public Follower Create(Follower follower)
        {
            follower.idFollower = dao.create(follower);
            return follower;
        }

        public int Delete(string idFollower)
        {
            return dao.delete(idFollower);
        }

        public Follower Get(String idUser, String IDUserFollower)
        {
            return dao.Get(idUser,IDUserFollower);
        }

        public List<Follower> GetAll(string idPlace)
        {
            UserDao daoUser = new UserDao();
            List<Follower> followers = dao.getAll(idPlace);
            for (int i = 0; i < followers.Count; i++)
            {
                followers[i].user = daoUser.ObtainUserbyId(followers[i].idUserFollower);
            }
            return followers;
        }

        public List<Follower> GetAllFollows(string idUserFollow)
        {
            UserDao daoUser = new UserDao();
            List<Follower> followers = dao.getAllFollows(idUserFollow);
            for (int i = 0; i < followers.Count; i++)
            {
                followers[i].user = daoUser.ObtainUserbyId(followers[i].idUserFollower);
            }
            return followers;
        }

        public int GetCount(string idUser)
        {
            return dao.getCount(idUser);
        }
    }
}
