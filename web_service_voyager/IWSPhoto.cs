﻿using System;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using web_service_voyager.Domain;

namespace web_service_voyager
{
    [ServiceContract]
    public interface IWSPhoto
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Create", ResponseFormat = WebMessageFormat.Json)]
        int Create(Photo photo);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetPhoto/{idPhoto}", ResponseFormat = WebMessageFormat.Json)]
        Stream GetPhoto(String idPhoto);
    }
}
