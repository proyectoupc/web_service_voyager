﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using web_service_voyager.Domain;

namespace web_service_voyager
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IWSFollower" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IWSFollower
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAll/{idUser}", ResponseFormat = WebMessageFormat.Json)]
        List<Follower> GetAll(String idUser);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAllFollows/{idUserFollow}", ResponseFormat = WebMessageFormat.Json)]
        List<Follower> GetAllFollows(String idUserFollow);
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Get/{idUser}/{IDUserFollower}", ResponseFormat = WebMessageFormat.Json)]
        Follower Get(String idUser, String IDUserFollower);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Create", ResponseFormat = WebMessageFormat.Json)]
        Follower Create(Follower follower);

        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "Delete/{idFollower}", ResponseFormat = WebMessageFormat.Json)]
        int Delete(string idFollower);
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetCount/{idUser}", ResponseFormat = WebMessageFormat.Json)]
        int GetCount(String idUser);
    }
}
