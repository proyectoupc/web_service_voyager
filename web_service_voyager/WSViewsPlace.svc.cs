﻿using web_service_voyager.Domain;
using web_service_voyager.Persistence;

namespace web_service_voyager
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "WSViewsPlace" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione WSViewsPlace.svc o WSViewsPlace.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class WSViewsPlace : IWSViewsPlace
    {
        ViewsPlaceDao dao = new ViewsPlaceDao();
        public ViewsPlace Create(ViewsPlace viewPlace)
        {
            //ViewsPlace viewPlaceCreated = dao.get(viewPlace.idPlace + "", viewPlace.idUser + "");
            //if (viewPlaceCreated == null)
            //{
            viewPlace = dao.create(viewPlace);
            //}
            viewPlace.countViews = dao.getCount(viewPlace.idPlace+"");
            return viewPlace;
        }

        public int Delete(string idViewsPlace)
        {
            return dao.delete(idViewsPlace);
        }

        public ViewsPlace Get(string idPlace, string idUser)
        {
            return dao.get(idPlace, idUser);
        }
    }
}
