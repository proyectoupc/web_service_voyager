﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using web_service_voyager.Domain;
using web_service_voyager.Persistence;

namespace web_service_voyager
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "WSComment" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione WSComment.svc o WSComment.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class WSComment : IWSComment
    {
        CommentDao dao = new CommentDao();
        public int Create(Comment comment)
        {
            int save = dao.create(comment);
            if (save > 0)
            {
                comment.idComentario = save;
                Notification notification = new Notification();
                PlaceDao placeDao = new PlaceDao();
                UserDao userDao = new UserDao();

                Place place = placeDao.getById(comment.idPlace + "");
                place.user = userDao.ObtainUserbyId(place.idUser);
                place.idPhotos = placeDao.getPhotos(place.idUser + "");
                notification.place = place;
                notification.user = userDao.ObtainUserbyId(comment.idUser);
                notification.comment = comment;

                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "POST";
                tRequest.ContentType = "application/json";
                tRequest.Headers.Add(string.Format("Authorization: key={0}", "AIzaSyDBGSMsfFU2OwHLBw9ZMQfv1PCrB849kCo"));
                JsonHeadFCM jsonHead = new JsonHeadFCM();
                JsonDataFCM jsonData = new JsonDataFCM();
                jsonData.my_custom_key = JsonConvert.SerializeObject(notification).ToString();
                jsonData.my_custom_key2 = true;
                jsonHead.data = jsonData;
                List<String> registration_ids = new List<string>();
                registration_ids.Add(place.user.token);
                jsonHead.registration_ids = registration_ids;
                String postbody = JsonConvert.SerializeObject(jsonHead).ToString();
                Byte[] byteArray = Encoding.UTF8.GetBytes(postbody);
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            if (dataStreamResponse != null) using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                {
                                    String sResponseFromServer = tReader.ReadToEnd();
                                    //result.Response = sResponseFromServer;
                                }
                        }
                    }
                }
            }
            return save;
        }

        public int Delete(string idComment)
        {
            return dao.delete(idComment);
        }

        public List<Comment> GetAll(string idPlace)
        {
            List<Comment> coments = dao.getAll(idPlace);
            UserDao userDao = new UserDao();
            for (int i = 0; i < coments.Count; i++)
            {
                coments[i].user = userDao.ObtainUserbyId(coments[i].idUser);
            }
            return coments;
        }
    }
}
