﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using web_service_voyager.Domain;

namespace web_service_voyager
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IWSComment" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IWSComment
    {

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAll/{idPlace}", ResponseFormat = WebMessageFormat.Json)]
        List<Comment> GetAll(String idPlace);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Create", ResponseFormat = WebMessageFormat.Json)]
        int Create(Comment comment);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "Delete/{idComment}", ResponseFormat = WebMessageFormat.Json)]
        int Delete(string idComment);
    }
}
