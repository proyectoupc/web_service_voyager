﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using web_service_voyager.Dominio;
using web_service_voyager.Persistence;

namespace web_service_voyager
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "WSUser" en el código, en svc y en el archivo de configuración a la vez.
    // NOTA: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione WSUser.svc o WSUser.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class WSUser : IWSUser
    {
        private UserDao dao = new UserDao();

        public Stream GetBackgroundPhoto(string idUser)
        {
            MemoryStream ms = new MemoryStream(dao.getBackgroundPhotoById(idUser));
            WebOperationContext.Current.OutgoingRequest.Headers.Add("Slug", "title");
            WebOperationContext.Current.OutgoingRequest.Method = "GET";
            WebOperationContext.Current.OutgoingResponse.ContentType = "image/jpeg";
            return ms;
        }

        public Stream GetPhoto(string idUser)
        {
            MemoryStream ms = new MemoryStream(dao.getPhotoById(idUser));
            WebOperationContext.Current.OutgoingRequest.Headers.Add("Slug", "title");
            WebOperationContext.Current.OutgoingRequest.Method = "GET";
            WebOperationContext.Current.OutgoingResponse.ContentType = "image/jpeg";
            return ms;
        }

        public User Login(User user)
        {
            return dao.login(user);
        }

        public User ObtainUserbyTokenReocveryPassword(string tokenRecoveryPassword)
        {
            return dao.ObtainUserbyTokenReocveryPassword(tokenRecoveryPassword);
        }

        public User Register(User user)
        {
            return dao.Register(user);
        }

        public int RestorePassword(string userEmail)
        {
            Int32 unixTimestamp = (Int32)(DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            String tokenRecovery = string.Join("", SHA1CryptoServiceProvider.Create().ComputeHash(Encoding.UTF8.GetBytes(unixTimestamp + "")).Select(x => x.ToString("X2"))).ToUpper();
            int respone = dao.updateTokenRecoveryPassword(userEmail, tokenRecovery);
            if (respone == 1)
            {
                User user = dao.ObtainUserbyEmail(userEmail);
                if (user != null)
                {
                    var fromAddress = new MailAddress("voyager.adm@gmail.com", "VOYAGER");
                    var toAddress = new MailAddress(userEmail, String.Format("{0} {1}", user.name, user.lastName));
                    const string fromPassword = "Ordonez#159";
                    const string subject = "Restablece la contraseña de tu cuenta de VOYAGER";
                    StringBuilder body = new StringBuilder();
                    body.Append("Hola ");
                    body.Append(user.name);
                    body.Append(".<br>");
                    body.Append("Solicitaste la recuperación de tu contraseña de tu cuenta de VOYAGER, por favor ingresa al siguiente en enlace para poder restablecer. ");
                    body.Append("<a href='https://voyager-web.azurewebsites.net/RecoveryPassword.aspx?token=");
                    body.Append(tokenRecovery);
                    body.Append("'>");
                    body.Append("INGRESE AQUÍ");
                    body.Append("</a>");
                    body.Append("<br>");
                    body.Append("<br>");
                    body.Append("<b> Atte.<br> Equipo VOYAGER </b><br>");

                    var smtp = new SmtpClient
                    {
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                    };
                    using (var message = new MailMessage(fromAddress, toAddress)
                    {
                        Subject = subject,
                        IsBodyHtml = true,
                        Body = body.ToString(),
                    })
                    {
                        smtp.Send(message);
                    }
                }
                else
                {
                    return 0;
                }
            }
            return respone;
        }

        public User Update(User user)
        {
            return dao.Update(user);
        }

        public User UpdatePassword(User user)
        {
            return dao.UpdatePassword(user);
        }

        public int UpdateToken(User user)
        {
            return dao.UpdateToken(user);
        }

        public int updateTokenRecoveryPassword(User user)
        {
            return dao.updateTokenRecoveryPassword(user.email, user.token);
        }
    }
}
