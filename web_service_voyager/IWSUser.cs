﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using web_service_voyager.Dominio;

namespace web_service_voyager
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IWSUser" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IWSUser
    {
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Login", ResponseFormat = WebMessageFormat.Json)]
        User Login(User user);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Register", ResponseFormat = WebMessageFormat.Json)]
        User Register(User user);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Update", ResponseFormat = WebMessageFormat.Json)]
        User Update(User user);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdatePassword", ResponseFormat = WebMessageFormat.Json)]
        User UpdatePassword(User user);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetPhoto/{idUser}", ResponseFormat = WebMessageFormat.Json)]
        Stream GetPhoto(String idUser);
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "RestorePassword/{userEmail}", ResponseFormat = WebMessageFormat.Json)]
        int RestorePassword(String userEmail);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetBackgroundPhoto/{idUser}", ResponseFormat = WebMessageFormat.Json)]
        Stream GetBackgroundPhoto(String idUser);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateToken", ResponseFormat = WebMessageFormat.Json)]
        int UpdateToken(User user);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "ObtainUserbyTokenReocveryPassword/{tokenRecoveryPassword}", ResponseFormat = WebMessageFormat.Json)]
        User ObtainUserbyTokenReocveryPassword(String tokenRecoveryPassword);
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "updateTokenRecoveryPassword", ResponseFormat = WebMessageFormat.Json)]
        int updateTokenRecoveryPassword(User user);

    }
}
