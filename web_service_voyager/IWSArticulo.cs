﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using web_service_voyager.Domain;

namespace web_service_voyager
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IWSArticulo" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IWSArticulo
    {

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAll", ResponseFormat = WebMessageFormat.Json)]
        List<Articulo> GetAll();

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Create", ResponseFormat = WebMessageFormat.Json)]
        int Create(Articulo comment);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Update", ResponseFormat = WebMessageFormat.Json)]
        int Update(Articulo comment);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "Delete/{idArticulo}", ResponseFormat = WebMessageFormat.Json)]
        int Delete(string idArticulo);
    }
}
