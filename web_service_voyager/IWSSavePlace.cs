﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using web_service_voyager.Domain;

namespace web_service_voyager
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IWSSavePlace" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IWSSavePlace
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAll/{idUser}", ResponseFormat = WebMessageFormat.Json)]
        List<SavePlace> GetAll(String idUser);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Create", ResponseFormat = WebMessageFormat.Json)]
        int Create(SavePlace savePlace);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "Delete/{idSavePlace}", ResponseFormat = WebMessageFormat.Json)]
        int Delete(string idSavePlace);
    }
}
