﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using web_service_voyager.Domain;

namespace web_service_voyager
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de interfaz "IWSRatingPlace" en el código y en el archivo de configuración a la vez.
    [ServiceContract]
    public interface IWSRatingPlace
    {

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "Get/{idPlace}/{idUser}", ResponseFormat = WebMessageFormat.Json)]
        RatingPlace Get(String idPlace, String idUser);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "Create", ResponseFormat = WebMessageFormat.Json)]
        RatingPlace Create(RatingPlace ratingPlace);
        [OperationContract]
        [WebInvoke(Method = "DELETE", UriTemplate = "Delete/{idRatingPlace}/{idPlace}", ResponseFormat = WebMessageFormat.Json)]
        Int64 Delete(string idRatingPlace,String idPlace);
    }
}
